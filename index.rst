

.. https://framapiaf.org/web/tags/qualite.rss
.. https://framapiaf.org/web/tags/softwareQuality.rss

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>


|FluxWeb| `RSS <http://linkertree.frama.io/pvergain/rss.xml>`_

.. _linkertree_pvergain:

=====================================================================
Software worker, #freedom, #justice, #solidarity. #Empathie first
=====================================================================

- https://framapiaf.org/@pvergain, https://framapiaf.org/@pvergain.rss
- https://babka.social/@pvergain, https://babka.social/@pvergain.rss
- https://rstockm.github.io/mastowall/?hashtags=grenoble&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=annecy&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org


Fedipack logiciel libre (Les comptes français de Mastodon qui postent régulièrement des informations utiles sur les logiciels libres et open source)
========================================================================================================================================================

- https://fedidevs.com/s/NDQz/

Liens infos
=========================

- https://gdevops.frama.io/liens/infos-2025
- https://gdevops.frama.io/liens/infos (2021 à 2024)


Liens raindrop (pour tests, intéressant pour les non-informaticiens)
========================================================================

- https://raindrop.io/pvergain
- https://raindrop.io/pvergain/python-51985474

**Antiracisme (antisemitisme, antitsiganisme, islamophobie)**
==========================================================================

- https://antiracisme.frama.io/linkertree/
- https://antiracisme.frama.io/infos-2024/
- https://antiracisme.frama.io/infos-2023/
- https://antiracisme.frama.io/infos/
- https://luttes.frama.io/contre/l-antisemitisme/
- https://luttes.frama.io/contre/l-antitsiganisme/
- http://collectif-golem.frama.io/golem

LDH GT discrminations
------------------------

- https://ldh.frama.io/gt-discriminations/



Précarité magazine
-------------------

- http://un-autre-futur.frama.io/precarite-magazine-2024

Leftrenewal (Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche)
==================================================================================================================================

- https://leftrenewal.frama.io/linkertree/
- https://leftrenewal.frama.io/leftrenewal-info/

Logiciel libre
=======================

- https://rstockm.github.io/mastowall/?hashtags=logicielLibre,OpenSource&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=linux&server=https://framapiaf.org
- https://luttes.frama.io/pour/le-logiciel-libre/
- https://metro-grenoble.frama.io/logiciel-libre/infos/
- https://alposs.frama.io/alposs-2025/
- https://alposs.frama.io/alposs-2024/
- https://alposs.frama.io/infos/

Openstreetmap
===================

- https://rstockm.github.io/mastowall/?hashtags=osm&server=https://framapiaf.org
- https://luttes.frama.io/pour/le-libre/opendata/openstreetmap/
- https://luttes.frama.io/pour/le-libre/opendata/openstreetmap-2023/

Liens Documentation
====================

- https://rstockm.github.io/mastowall/?hashtags=documentation,sphinx&server=https://framapiaf.org
- https://gdevops.frama.io/documentation/linkertree/
- https://gdevops.frama.io/documentation/news/
- https://gdevops.frama.io/documentation/tools/
- https://gdevops.frama.io/documentation/sphinx/
- https://gdevops.frama.io/documentation/sphinx-demos/
- https://gdevops.frama.io/documentation/formats/
- https://gdevops.frama.io/documentation/tuto/


Liens Python
=======================

- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org
- https://gdevops.frama.io/python/linkertree/
- https://gdevops.frama.io/python/versions/
- https://gdevops.frama.io/python/news-2025/
- https://gdevops.frama.io/python/news-2024/
- https://gdevops.frama.io/python/news-2023/
- https://gdevops.frama.io/python/news/
- https://gdevops.frama.io/python/tuto/

Liens Databases
====================

- https://rstockm.github.io/mastowall/?hashtags=postgresql&server=https://framapiaf.org
- https://gdevops.frama.io/databases/linkertree/
- https://gdevops.frama.io/databases/tuto/
- https://gdevops.frama.io/databases/news/
- https://gdevops.frama.io/databases/postgresql/

Liens Django
==============

- https://rstockm.github.io/mastowall/?hashtags=django&server=https://framapiaf.org
- https://awesomedjango.org/
- https://gdevops.frama.io/django/linkertree/
- https://gdevops.frama.io/django/versions/
- https://gdevops.frama.io/django/tuto/
- https://gdevops.frama.io/django/news/
- https://gdevops.frama.io/django/tools/

Liens opsindev
==============================

- https://rstockm.github.io/mastowall/?hashtags=gitlab,git&server=https://framapiaf.org
- https://gdevops.frama.io/opsindev/linkertree/
- https://gdevops.frama.io/opsindev/news/
- https://gdevops.frama.io/opsindev/tuto-project/ (gitlab)
- https://gdevops.frama.io/opsindev/sysops/ (Devops/sysops, fabric, glossaire)
- https://gdevops.frama.io/opsindev/tuto-git/
- https://gdevops.frama.io/opsindev/ansible/
- https://gdevops.frama.io/opsindev/dagger/
- https://gdevops.frama.io/opsindev/kubernetes/
- https://gdevops.frama.io/opsindev/secops/
- https://gdevops.frama.io/opsindev/tutorial/

Liens dev
=================

- https://rstockm.github.io/mastowall/?hashtags=rust&server=https://framapiaf.org
- https://gdevops.frama.io/dev/linkertree/
- https://gdevops.frama.io/dev/tuto-cli/
- https://gdevops.frama.io/dev/tuto-build/
- https://gdevops.frama.io/dev/tuto-programming/
- https://gdevops.frama.io/dev/tuto-languages/
- https://gdevops.frama.io/dev/tuto-lowtech/
- https://gdevops.frama.io/dev/tuto-os/
- https://gdevops.frama.io/dev/tuto-rust/

Liens dev/ides
=================

- https://rstockm.github.io/mastowall/?hashtags=jupyterlab,jupyterlite&server=https://framapiaf.org
- https://gdevops.frama.io/dev/tuto-ides/


Liens web
=====================

- https://rstockm.github.io/mastowall/?hashtags=wasi,html5&server=https://framapiaf.org
- https://gdevops.frama.io/web/linkertree/
- https://gdevops.frama.io/web/news/
- https://gdevops.frama.io/web/accessibilite/
- https://gdevops.frama.io/web/fediverse/
- https://gdevops.frama.io/web/tuto-http/
- https://gdevops.frama.io/web/tuto-html/
- https://gdevops.frama.io/web/htmx/
- https://gdevops.frama.io/web/tuto-css/
- https://gdevops.frama.io/web/javascript/
- https://gdevops.frama.io/web/hyperscript/
- https://gdevops.frama.io/web/tuto-rss/
- https://gdevops.frama.io/web/components/
- https://gdevops.frama.io/web/webassembly/
- https://gdevops.frama.io/web/frameworks/




LLMs (Large Language Models/Grands modèles de langue)
=========================================================

- https://rstockm.github.io/mastowall/?hashtags=openllm&server=https://framapiaf.org

- https://gdevops.frama.io/ia/linkertree
- https://gdevops.frama.io/ia/llms/
- https://gdevops.frama.io/ia/openllm-france/

Hacktivistes
==============

- https://hacktivistes.frama.io/aaron-swartz/


Bullshits (blockchain)
==========================

- https://luttes.frama.io/contre/blockchain/


Liens Grenoble (AIAK, Barathym, Cercle Bernard Lazare, Musée de la Résistance de l’Isère)
=============================================================================================

- https://rstockm.github.io/mastowall/?hashtags=grenoble&server=https://framapiaf.org

- https://grenoble.frama.io/linkertree/
- https://grenoble.frama.io/luttes-2024/
- https://grenoble.frama.io/luttes-2023/
- https://grenoble.frama.io/luttes/retraites/
- https://metro-grenoble.frama.io/logiciel-libre/infos/

AIAK
-----------

- https://aiak.frama.io/aiak-info/

Barathym
--------------

- https://barathym.frama.io/linkertree
- https://barathym.frama.io/barathym-2024

Cercle Bernard Lazare
----------------------------

- https://cbl.frama.io/cbl-grenoble/linkertree/
- https://cbl.frama.io/cbl-grenoble/infos/
- https://cbl.frama.io/cbl-grenoble/decennie-2020/


Musée de la Résistance de l’Isère
--------------------------------------

- http://grenoble.frama.io/musee-de-la-resistance-et-de-la-deportation-de-l-isere


Pour la paix
===============

- https://luttes.frama.io/pour/la-paix/linkertree/
- https://judaism.gitlab.io/floriane_chinsky
- https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/
- https://luttes.frama.io/pour/la-paix/parentscirclefriends (Apeirogon, Grenoble)
- https://www.lapaixmaintenant.org/
- https://www.standing-together.org/about-us
- https://www.instagram.com/standing.together.movement/
- https://www.instagram.com/standing.together.english/
- https://www.youtube.com/@standingtogether7105/videos
- https://leftodon.social/@omdimbeyachad
- https://luttes.frama.io/pour/la-paix/womenwagepeace
- https://en.wikipedia.org/wiki/Women_of_the_Sun_(movement)


Justice climatique
=====================

- https://luttes.frama.io/pour/la-justice-climatique/index.html
- https://luttes.frama.io/pour/la-justice-climatique/orgas/ipcc/reports/ar6/ar6.html


Musée de la Résistance de l'Isère
=====================================

- http://grenoble.frama.io/musee-de-la-resistance-et-de-la-deportation-de-l-isere


Citoyens Résistants d'Hier et d'Aujourd'hui (CRHA)  Glières, Haute-Savoie
=============================================================================

- https://rstockm.github.io/mastowall/?hashtags=glieres&server=https://framapiaf.org
- https://glieres.frama.io/crha/linkertree/
- https://glieres.frama.io/crha/resistances-2024/
- https://glieres.frama.io/crha/resistances-2023/
- https://glieres.frama.io/crha/resistances/

luttes/pour/les-droits-humains/cncdh
=========================================

- https://luttes.frama.io/pour/les-droits-humains/cncdh/

luttes/pour/les-droits-humains
=======================================

- https://luttes.frama.io/pour/les-droits-humains/linkertree/

Luttes contre les extrêmes droites
==========================================

- https://framagit.org/luttes/contre/les-extremes-droites


Contre l'illiberalisme
==========================

- https://luttes.frama.io/contre/l-illiberalisme/

Contre l'antisémitisme
===========================

- https://luttes.frama.io/contre/l-antisemitisme/

Contre les media réactionnaires
=================================

- https://luttes.frama.io/contre/les-media-reactionnaires/

Luttes féministes en Iran
=========================================

- https://iran.frama.io/linkertree/
- https://iran.frama.io/luttes-2025/
- https://iran.frama.io/luttes-2024/
- https://iran.frama.io/luttes-2023/
- https://iran.frama.io/luttes/ (2022)
- https://bahareh-akrami.frama.io/iran/
- https://femme-vie-liberte.frama.io/expositions-2023/grenoble/
- https://femme-vie-liberte.frama.io/expositions-2024/seyssinet-pariset/


Luttes féministes au Kurdistan
=========================================

- https://kurdistan.frama.io/linkertree/
- https://aiak.frama.io/aiak-info/

Luttes féministes au Rojava
=========================================

- https://rojava.frama.io/luttes/


Judaïsme
=============

- https://judaism.gitlab.io/linkertree/
- https://judaism.gitlab.io/judaisme/
- https://judaism.gitlab.io/judaisme-2024
- https://judaism.gitlab.io/judaisme-2023/
- https://judaism.gitlab.io/judaisme-2020/
- https://judaism.gitlab.io/judaisme-2019/
- https://judaism.gitlab.io/floriane_chinsky/
- https://judaism.gitlab.io/bokertov/
- https://judaism.gitlab.io/shabbats/


Mes comptes mastodon
====================

- https://framapiaf.org/@pvergain
- https://framapiaf.org/@pvergain.rss (flux Web)
- https://babka.social/@pvergain
- https://babka.social/@pvergain.rss (flux Web)

Mon compte pixelfed
=====================

- https://pixelfed.photos/i/web/profile/494783005213231156
- https://pixelfed.photos/users/Patrick.atom


Coin musée ? : mon ancien blog wordpress
===========================================

- https://pvergain.wordpress.com/

TDAH
=====

- https://gtdah.frama.io/tdah/

Conflit Israël/Palestine
==================================

- https://conflits.frama.io/israel-palestine/linkertree/
- https://conflits.frama.io/israel-palestine/israel-palestine-2024/
- https://conflits.frama.io/israel-palestine/israel-palestine-2023/
- https://conflits.frama.io/israel-palestine/israel-palestine
- https://israel.frama.io/sionisme-antisionisme


Ukraine
============

- https://ukraine.frama.io/linkertree
- https://ukraine.frama.io/luttes-2024
- https://ukraine.frama.io/luttes-2023
- https://ukraine.frama.io/luttes
- https://ukraine.frama.io/media-2023
- https://ukraine.frama.io/media-2022
